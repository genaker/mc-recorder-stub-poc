package net.genaker.mcrecorder.constants;

public enum SessionTypeEnum {
    TYPE_ADHOC("TYPE_ADHOC"),
    TYPE_PRIVATE("TYPE_PRIVATE"),
    TYPE_VOIP("TYPE_VOIP"),
    TYPE_PARTICIPATING("TYPE_PARTICIPATING"),
    TYPE_PREARRANGED("TYPE_PREARRANGED"),
    TYPE_EMERGENCY_PREARRANGED("TYPE_EMERGENCY_PREARRANGED"),
    TYPE_CHAT("TYPE_CHAT"),
    TYPE_EMERGENCY_CHAT("TYPE_EMERGENCY_CHAT"),
    TYPE_VIDEO_CHAT("TYPE_VIDEO_CHAT");

    private String keyName;

    private SessionTypeEnum(String keyName) {
        this.keyName = keyName;
    }

    public String getKeyName() {
        return keyName;
    }

    public static SessionTypeEnum map(String key) {
        return SessionTypeEnum.valueOf(key.replace(' ', '_').toUpperCase());
    }
}