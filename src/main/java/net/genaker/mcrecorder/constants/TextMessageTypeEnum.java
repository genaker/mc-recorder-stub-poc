package net.genaker.mcrecorder.constants;

public enum TextMessageTypeEnum {
    TYPE_FILE("FILE"),
    TYPE_TEXT("TEXT");

    private String keyName;

    private TextMessageTypeEnum(String keyName) {
        this.keyName = keyName;
    }

    public String getKeyName() {
        return keyName;
    }

    public static TextMessageTypeEnum map(String key) {
        return TextMessageTypeEnum.valueOf(key.replace(' ', '_').toUpperCase());
    }
}