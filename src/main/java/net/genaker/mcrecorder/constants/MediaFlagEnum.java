package net.genaker.mcrecorder.constants;

public enum MediaFlagEnum {
    NO("NO"),
    FULL("FULL"),
    EVENTS("EVENTS");

    private String keyName;

    private MediaFlagEnum(String keyName) {
        this.keyName = keyName;
    }

    public String getKeyName() {
        return keyName;
    }

    public static MediaFlagEnum map(String key) {
        return MediaFlagEnum.valueOf(key.replace(' ', '_').toUpperCase());
    }
}