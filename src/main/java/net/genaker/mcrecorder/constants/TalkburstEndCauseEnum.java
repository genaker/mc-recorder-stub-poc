package net.genaker.mcrecorder.constants;

public enum TalkburstEndCauseEnum {
    NORMAL("NORMAL"),
    TOO_LONG("TOO_LONG"),
    PREEMPTED("PREEMPTED"),
    OTHER("OTHER");

    private String keyName;

    private TalkburstEndCauseEnum(String keyName) {
        this.keyName = keyName;
    }

    public String getKeyName() {
        return keyName;
    }

    public static TalkburstEndCauseEnum map(String key) {

        return TalkburstEndCauseEnum.valueOf(key.replace(' ', '_').toUpperCase());
    }
}