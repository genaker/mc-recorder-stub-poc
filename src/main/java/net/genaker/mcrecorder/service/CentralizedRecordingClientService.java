package net.genaker.mcrecorder.service;

import lombok.extern.slf4j.Slf4j;
import net.genaker.mcrecorder.constants.MediaFlagEnum;
import net.genaker.mcrecorder.constants.SessionTypeEnum;
import net.genaker.mcrecorder.constants.TalkburstEndCauseEnum;
import net.genaker.mcrecorder.constants.TextMessageTypeEnum;
import net.genaker.mcrecorder.dispatcher.DatagramUdpDispatcher;
import net.genaker.mcrecorder.dispatcher.PacketTcpDispatcher;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PreDestroy;
import java.nio.ByteBuffer;

@Service
@Slf4j
public class CentralizedRecordingClientService {

    private static final String CRC_TLKSTART_HEADER = "CRC-TLKSTR:";
    private static final String CRC_TLKEND_HEADER ="CRC-TLKEND:";
    private static final String CRC_TEXTMESSAGE_HEADER = "CRC-TXTMSG:";
    private static final String CRC_VIDEOSTART_HEADER = "CRC-VIDSTR:";
    private static final String CRC_VIDEOEND_HEADER = "CRC-VIDSTP:";
    private static final String CRC_VOIPSTART_HEADER = "CRC-VIPSTR:";
    private static final String CRC_VOIPEND_HEADER = "CRC-VIPEND:";


    private final PacketTcpDispatcher tcpDispatcher;
    DatagramUdpDispatcher udpDispatcher;
    private final MediaFlagEnum mediaFlag;

    public CentralizedRecordingClientService(@Value("${genaker.mcRecorder.client.media}") String mediaProperty,
                                                    PacketTcpDispatcher packetTcpDispatcher,
                                                    DatagramUdpDispatcher datagramUdpDispatcher) {

        this.mediaFlag = MediaFlagEnum.valueOf(mediaProperty);
        this.tcpDispatcher = packetTcpDispatcher;
        this.udpDispatcher = datagramUdpDispatcher;
    }
    public MediaFlagEnum getMediaFlag(){
        return mediaFlag;
    }

    public void callOnTalkBurstStart(long ssrc,
                                    long talkBurstStartTimestamp,
                                    String userUriStart,
                                    String callId,
                                    String organizationName,
                                    SessionTypeEnum sessionType,
                                    String sessionUri,
                                    String groupName,
                                    String receivingParticipants,
                                    String callInitiator,
                                    String callParticipants,
                                    int numberOfAffiliated,
                                    String cryptoKey,
                                    String rtpCodec) {

        if(MediaFlagEnum.NO.equals(mediaFlag)){
            log.info("Current configuration of CRC does not sent events to CRS");
        }else{
            log.info("Process callOnTalkBurstStart to generate byte[] to send to socket.");
            String headerStart = CRC_TLKSTART_HEADER;
            ByteBuffer byteBuffer = ByteBuffer.allocate(
                    headerStart.length() +                  //header
                        32 +                                //data lengths
                        8 +                                 //ssrc Long.BYTES = 8
                        8 +                                 //talkBurstStartTimestamp Long.BYTES = 8
                        userUriStart.length() +             //userUri
                        callId.length() +                   //callId
                        organizationName.length() +         //organizationName
                        sessionType.toString().length() +   //sessionType
                        sessionUri.length() +               //sessionUri
                        groupName.length()+                 //groupName
                        receivingParticipants.length()+     //receivingParticipants
                        callInitiator.length() +            // callInitiator
                        callParticipants.length()+          //callParticipants
                        mediaFlag.toString().length() +     //mediaFlag
                        4 +                                 //numberOfAffiliated Integer.BYTES = 4
                        cryptoKey.length() +                //cryptoKey
                        rtpCodec.length()                   //rtpCodec
            );

            byteBuffer
                .put(headerStart.getBytes())
                .putShort((short)8)                         // ssrc length Long.BYTES = 8
                .putShort((short)8)                         // talkBurstStartTimestamp length Long.BYTES = 8
                .putShort((short)userUriStart.length())     // userUri length
                .putShort((short)callId.length())           // callId length
                .putShort((short)organizationName.length()) // organizationName length
                .putShort((short)sessionType.toString().length())       // sessionType length
                .putShort((short)sessionUri.length())       // sessionUri length
                .putShort((short)groupName.length())         // groupName length
                .putShort((short)receivingParticipants.length()) //receivingParticipants length
                .putShort((short)callInitiator.length())     // callInitiator length
                 .putInt(callParticipants.length())  // callParticipants length
                //.putShort((short)callParticipants.length())  // callParticipants length
                .putShort((short)mediaFlag.toString().length())         // mediaFlag length
                .putShort((short)4)                         // numberOfAffiliated length Integer.BYTES = 4
                .putShort((short)cryptoKey.length())        // cryptoKey length
                .putShort((short)rtpCodec.length())         // rtpCodec length
                .putLong(ssrc)
                .putLong(talkBurstStartTimestamp)
                .put(userUriStart.getBytes())
                .put(callId.getBytes())
                .put(organizationName.getBytes())
                .put(sessionType.toString().getBytes())
                .put(sessionUri.getBytes())
                .put(groupName.getBytes())
                .put(receivingParticipants.getBytes())
                .put(callInitiator.getBytes())
                .put(callParticipants.getBytes())
                .put(mediaFlag.toString().getBytes())
                .putInt(numberOfAffiliated)
                .put(cryptoKey.getBytes())
                .put(rtpCodec.getBytes());
            byte[]messageContent = byteBuffer.array();
            log.info("Go to send message to tcpDispatcher length:"+messageContent.length);
            tcpDispatcher.send(messageContent);
        }
    }

    public void callOnTalkBurstEnd(long ssrc, long talkBurstEndTimestamp, TalkburstEndCauseEnum cause) {
        if(MediaFlagEnum.NO.equals(mediaFlag)){
            log.info("Current configuration of CRC does not sent events to CRS");
        }else{
            log.info("Process callOnTalkBurstEnd to generate byte[] to send to socket.");

            ByteBuffer byteBuffer = ByteBuffer.allocate(
                    CRC_TLKEND_HEADER.length() +        //header
                            6 +                         //data lengths
                            8 +                         //ssrc Long.BYTES = 8
                            8 +                         //talkBurstEndTimestamp Long.BYTES = 8
                            cause.name().length()           //endCause
            );

            byteBuffer
                    .put(CRC_TLKEND_HEADER.getBytes())
                    .putShort((short) 8)                 // ssrc length Long.BYTES = 8
                    .putShort((short) 8)                 // talkBurstEndTimestamp length Long.BYTES = 8
                    .putShort((short)cause.name().length())  // endCause length
                    .putLong(ssrc)                       // ssrc value
                    .putLong(talkBurstEndTimestamp)      // talkBurstEndTimestamp value
                    .put(cause.name().getBytes());           // endCause value
            byte[]messageContent = byteBuffer.array();
            log.info("Go to send message to tcpDispatcher length:"+messageContent.length);
            tcpDispatcher.send(messageContent);
        }
    }

    public void sendAudioRtpPacket(long ssrc, int sequenceNumber, long packetSsrc, byte[] burst) {
        if(MediaFlagEnum.FULL.equals(mediaFlag)){
            log.info("Send Audio Packet ssrc:"+ssrc+" sequence:"+sequenceNumber+" packetSsrc:"+packetSsrc);
            ByteBuffer byteBuffer = ByteBuffer.allocate(
                    8 + //ssrc Long.BYTES = 8
                    4 + //sequence number Integer.BYTES = 4
                    8 + //packet SSRC Long.BYTES = 8
                    4 + //burst.length Integer.BYTES = 4
                    burst.length
            );
            byteBuffer.putLong(ssrc);
            byteBuffer.putInt(sequenceNumber);
            byteBuffer.putLong(packetSsrc);
            byteBuffer.putInt(burst.length);
            byteBuffer.put(burst);
            udpDispatcher.send(byteBuffer.array());
        }else{
            log.info("Current configuration of CRC does not sent data to CRS");
        }
    }

    public void callOnNewTextMessage(long sendTextMessageTimestamp,
                                     String organizationName,
                                     String localUser,
                                     String localUserDisplay,
                                     String groupName,
                                     String remoteUser,
                                     String remoteUserDisplay,
                                     String message,
                                     String attachment,
                                     String attachmentFileName,
                                     String sipCallId,
                                     TextMessageTypeEnum messageType) {
        if(MediaFlagEnum.NO.equals(mediaFlag)){
            log.info("Current configuration of CRC does not sent events to CRS");
        }else {
            // Bug PS-557 send MEDIA_FLAG and full data of TextMessage and CRS decides if download attachment or not...
            log.info("Process callOnNewTextMessage to generate byte[] to send to socket.");

            ByteBuffer byteBuffer = ByteBuffer.allocate(
                    CRC_TEXTMESSAGE_HEADER.length() + //header
                    26 +              //data lengths
                    8 +              //long sendTextMessageTimestamp
                    organizationName.length() +
                    localUser.length() +
                    localUserDisplay.length() +
                    groupName.length() +
                    remoteUser.length() +
                    remoteUserDisplay.length() +
                    message.length() +
                    attachment.length() +
                    attachmentFileName.length() +
                    sipCallId.length() +
                    messageType.getKeyName().length() +
                    mediaFlag.getKeyName().length()
            );
            byteBuffer
                .put(CRC_TEXTMESSAGE_HEADER.getBytes())
                .putShort((short) 8) //sendTextMessageTimestamp
                .putShort((short) organizationName.length())
                .putShort((short) localUser.length())
                .putShort((short) localUserDisplay.length())
                .putShort((short) groupName.length())
                .putShort((short) remoteUser.length())
                .putShort((short) remoteUserDisplay.length())
                .putShort((short) message.length())
                .putShort((short) attachment.length())
                .putShort((short) attachmentFileName.length())
                .putShort((short) sipCallId.length())
                .putShort((short) messageType.getKeyName().length())
                .putShort((short) mediaFlag.getKeyName().length())
                .putLong(sendTextMessageTimestamp)
                .put(organizationName.getBytes())
                .put(localUser.getBytes())
                .put(localUserDisplay.getBytes())
                .put(groupName.getBytes())
                .put(remoteUser.getBytes())
                .put(remoteUserDisplay.getBytes())
                .put(message.getBytes())
                .put(attachment.getBytes())
                .put(attachmentFileName.getBytes())
                .put(sipCallId.getBytes())
                .put(messageType.getKeyName().getBytes())
                .put(mediaFlag.getKeyName().getBytes())
            ;
            byte[] messageContent = byteBuffer.array();
            log.info("Go to send message to tcpDispatcher length:" + messageContent.length);
            tcpDispatcher.send(messageContent);

        }
    }

    public void callOnVideoStart(long ssrc,
                                 long videoStartTimestamp,
                                 String organizationName,
                                 String callerUser,
                                 String remoteUser,
                                 String groupName,
                                 String sessionType,
                                 String sipCallId,
                                 String remoteSipCallId) {
        if(MediaFlagEnum.NO.equals(mediaFlag)){
            log.info("Current configuration of CRC does not sent events to CRS");
        }else {
            log.info("Process callOnVideoStart to generate byte[] to send to socket.");

            ByteBuffer byteBuffer = ByteBuffer.allocate(
                    CRC_VIDEOSTART_HEADER.length() + //header
                            18 +              //data lengths
                            8 +              //ssrc
                            8 +              //video start timestamp
                            organizationName.length() +
                            callerUser.length() +
                            remoteUser.length() +
                            groupName.length() +
                            sessionType.length() +
                            sipCallId.length() +
                            remoteSipCallId.length()
            );
            byteBuffer
                    .put(CRC_VIDEOSTART_HEADER.getBytes())
                    .putShort((short) 8)
                    .putShort((short) 8)
                    .putShort((short) organizationName.length())
                    .putShort((short) callerUser.length())
                    .putShort((short) remoteUser.length())
                    .putShort((short) groupName.length())
                    .putShort((short) sessionType.length())
                    .putShort((short) sipCallId.length())
                    .putShort((short) remoteSipCallId.length())
                    .putLong(ssrc)
                    .putLong(videoStartTimestamp)
                    .put(organizationName.getBytes())
                    .put(callerUser.getBytes())
                    .put(remoteUser.getBytes())
                    .put(groupName.getBytes())
                    .put(sessionType.getBytes())
                    .put(sipCallId.getBytes())
                    .put(remoteSipCallId.getBytes());
            byte[] messageContent = byteBuffer.array();
            log.info("Go to send message to tcpDispatcher length:" + messageContent.length);
            tcpDispatcher.send(messageContent);
        }
    }

    public void callOnVideoEnd(long ssrc, long videoEndTimestamp) {
        if(MediaFlagEnum.NO.equals(mediaFlag)){
            log.info("Current configuration of CRC does not sent events to CRS");
        }else {
            log.info("Process callOnVideoEnd to generate byte[] to send to socket.");

            ByteBuffer byteBuffer = ByteBuffer.allocate(
                    CRC_VIDEOEND_HEADER.length() + //header
                            4 +               //data lengths
                            8 +               //ssrc
                            8                 //video end timestamp
            );
            byteBuffer
                    .put(CRC_VIDEOEND_HEADER.getBytes())
                    .putShort((short) 8)
                    .putShort((short) 8)
                    .putLong(ssrc)
                    .putLong(videoEndTimestamp);
            byte[] messageContent = byteBuffer.array();
            log.info("Go to send message to tcpDispatcher length:" + messageContent.length);
            tcpDispatcher.send(messageContent);
        }
    }

    public void callOnVoIpStart(long ssrc,
                                long voipStartTimestamp,
                                String organizationName,
                                String callerUser,
                                String callerCallId,
                                String remoteUser,
                                String remoteCallId,
                                String callerCryptoKey,
                                String remoteCryptoKey,
                                Integer firstRtpPacketSequenceNumber,
                                String rtpCodec) {
        if(MediaFlagEnum.NO.equals(mediaFlag)){
            log.info("Current configuration of CRC does not sent events to CRS");
        }else {
            log.info("Process callOnVoIpStart to generate byte[] to send to socket.");

            ByteBuffer byteBuffer = ByteBuffer.allocate(
                    CRC_VOIPSTART_HEADER.length() + //header
                            26 +              //data lengths
                            8 +               //ssrc
                            8 +               //voip start timestamp
                            organizationName.length() +
                            callerUser.length() +
                            callerCallId.length() +
                            remoteUser.length() +
                            remoteCallId.length() +
                            callerCryptoKey.length()+        //callerCryptoKey
                            remoteCryptoKey.length()+        //remoteCryptoKey
                            4 +                              //firstRtpPacketSequenceNumber
                            rtpCodec.length()                //rtpCodec
            );
            byteBuffer
                    .put(CRC_VOIPSTART_HEADER.getBytes())
                    .putShort((short) 8) //ssrc
                    .putShort((short) 8) //timestamp
                    .putShort((short) organizationName.length())
                    .putShort((short) callerUser.length())
                    .putShort((short) callerCallId.length())
                    .putShort((short) remoteUser.length())
                    .putShort((short) remoteCallId.length())
                    .putShort((short)callerCryptoKey.length())   // callerCryptoKey length
                    .putShort((short)remoteCryptoKey.length())   // remoteCryptoKey length
                    .putShort((short)4)                          // firstRtpPacketSequenceNumber length
                    .putShort((short)rtpCodec.length())          // rtpCodec length
                    .putLong(ssrc)
                    .putLong(voipStartTimestamp)
                    .put(organizationName.getBytes())
                    .put(callerUser.getBytes())
                    .put(callerCallId.getBytes())
                    .put(remoteUser.getBytes())
                    .put(remoteCallId.getBytes())
                    .put(callerCryptoKey.getBytes())
                    .put(remoteCryptoKey.getBytes())
                    .putInt(firstRtpPacketSequenceNumber)
                    .put(rtpCodec.getBytes())
            ;
            byte[] messageContent = byteBuffer.array();
            log.info("Go to send message to tcpDispatcher length:" + messageContent.length);
            tcpDispatcher.send(messageContent);
        }
    }

    public void callOnVoIpEnd(long ssrc, long voipEndTimestamp) {
        if(MediaFlagEnum.NO.equals(mediaFlag)){
            log.info("Current configuration of CRC does not sent events to CRS");
        }else {
            log.info("Process callOnVoIpEnd to generate byte[] to send to socket.");

            ByteBuffer byteBuffer = ByteBuffer.allocate(
                    CRC_VOIPEND_HEADER.length() + //header
                            4 +               //data lengths
                            8 +               //ssrc
                            8                 //voip end timestamp
            );
            byteBuffer
                    .put(CRC_VOIPEND_HEADER.getBytes())
                    .putShort((short) 8)
                    .putShort((short) 8)
                    .putLong(ssrc)
                    .putLong(voipEndTimestamp);
            byte[] messageContent = byteBuffer.array();
            log.info("Go to send message to tcpDispatcher length:" + messageContent.length);
            tcpDispatcher.send(messageContent);
        }
    }

    @PreDestroy
    public void tearDown() {

        this.tcpDispatcher.assureSocketClosureAndInterruptThread();
    }
}
