package net.genaker.mcrecorder.utils;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.*;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.ConcurrentHashMap;

@Component
@Slf4j
public class CustomSocketUtils {

    public static final int BUFFER_SIZE = 1024;
    public static final int LOW_BUFFER_SIZE = 128;

    public enum MonitorMode {UTIL, SERVER};

    private ConcurrentHashMap<Integer, MonitorMode> socketMonitor = new ConcurrentHashMap<>();

    public StringBuilder binaryReadIntMode(Socket socket) throws IOException {
        DataInputStream in = new DataInputStream(new BufferedInputStream(socket.getInputStream()));
        int length;
        try {
            length = in.readInt();
            if (length <= 0 || length > 2048 /*MAX_TCP_PACKET_SIZE */) {
                log.info("Malformed packet");
                return new StringBuilder();
            }
        } catch (java.io.EOFException e) {
            // Reached end of data
            log.debug("Reached end of data", e);
            return new StringBuilder();
        }
        byte[] messageByte = new byte[length];
        StringBuilder dataString = new StringBuilder(length);
        boolean end = false;
        int totalBytesRead = 0;
        while (!end) {
            int currentBytesRead = in.read(messageByte);
            totalBytesRead = currentBytesRead + totalBytesRead;
            if (totalBytesRead <= length) {
                dataString
                        .append(new String(messageByte, 0, currentBytesRead, StandardCharsets.UTF_8));
            } else {
                dataString
                        .append(new String(messageByte, 0, length - totalBytesRead + currentBytesRead,
                                StandardCharsets.UTF_8));
            }
            if (dataString.length() >= length) {
                end = true;
            }
        }
        return dataString;
    }

    public void binaryWriteIntMode(Socket s, byte[] data) throws IOException {
        /* DONT CLOSE THE SOCKET OUTPUTSTREAM (AVOID AUTOCLOSEABLE), OTHERWISE THE CLIENT WILL HAVE TO RECONNECT
         AND NO KEEP ALIVE POSSIBLE**/

        log.debug("Recorder::debug::binaryWriteIntMode::Go to send to socket. Length::" + data.length + "::" + s.getPort());
        //The problem is that DataOutputStream doesn't buffer.
        //wrap the original stream in a BufferedOutputStream or utilize BufferedOutputStream is more efficient.
        BufferedOutputStream bos = null;
        if (data.length > CustomSocketUtils.BUFFER_SIZE) {
            bos = new BufferedOutputStream(s.getOutputStream(), CustomSocketUtils.BUFFER_SIZE);
        } else if (data.length < CustomSocketUtils.LOW_BUFFER_SIZE) {
            bos = new BufferedOutputStream(s.getOutputStream(), CustomSocketUtils.LOW_BUFFER_SIZE);
        } else {
            bos = new BufferedOutputStream(s.getOutputStream());
        }
        //writeInt
        int var1 = data.length;
        bos.write(var1 >>> 24 & 255);
        bos.write(var1 >>> 16 & 255);
        bos.write(var1 >>> 8 & 255);
        bos.write(var1 & 255);
        //writeData
        bos.write(data);
        bos.flush();

        log.debug("Recorder::debug::binaryWriteIntMode::flush::Socket outputStream flushed::" + data.length + "::" + s.getPort());
    }

    /*
     * If socket is null or is closed not it's free
     * If socketMonitor have the flag a true it's free
     */
    public boolean socketIsFree(Socket socket) {
        return socket != null && !socket.isClosed() && !socketMonitor.containsKey(Integer.valueOf(socket.getPort()));
    }

    public StringBuilder binaryRead(Socket socket) throws IOException {
        if (socketIsFree(socket)) {
            putSocketMonitor(socket, CustomSocketUtils.MonitorMode.UTIL);
            try {
                return binaryReadIntMode(socket);
            } catch (IOException e) {
                log.warn("Socket error");
                throw e;
            } finally {
                removeSocketMonitor(socket, CustomSocketUtils.MonitorMode.UTIL);
            }
        } else {
            log.warn("Is imposible do a binary read, the socket is busy");
            return new StringBuilder();
        }
    }

    public void binaryWrite(Socket socket, byte[] data) throws IOException {
        if (socketIsFree(socket)) {
            putSocketMonitor(socket, CustomSocketUtils.MonitorMode.UTIL);
            try {
                binaryWriteIntMode(socket, data);
            } catch (IOException e) {
                log.warn("Socket error");
                throw e;
            } finally {
                removeSocketMonitor(socket, CustomSocketUtils.MonitorMode.UTIL);
            }
        }
    }

    public void putSocketMonitor(Socket socket, MonitorMode mode) {
        socketMonitor.put(socket.getPort(), mode);
    }

    public void removeSocketMonitor(Socket socket, MonitorMode myMode) {
        Integer port = socket.getPort();
        if (socketMonitor.containsKey(port) &&
                ((socketMonitor.get(port)).equals(myMode) || myMode.equals(MonitorMode.SERVER))) {
            socketMonitor.remove(socket.getPort());
        }
    }

    public Socket closeSocket(Socket socket) {
        if (socket == null) {
            return null;
        }
        socketMonitor.remove(socket.getPort());
        log.info("closeSocket:Try to close the socket");
        if (!socket.isClosed()) {
            try {
                socket.setSoTimeout(1);
            } catch (IOException ignored) {
            } finally {
                try {
                    socket.getOutputStream().close();
                } catch (IOException ignored) {
                } finally {
                    try (socket) {
                        socket.getInputStream().close();
                    } catch (IOException ignored) {
                    }
                }
            }
        }
        return socket;
    }

    public Socket agressiveCloseSocket(Socket socket) {
        socketMonitor.remove(socket.getPort());

        try {
            socket.setSoTimeout(1);
        } catch (IOException ignored) {
        } finally {
            try {
                socket.getOutputStream().close();
            } catch (IOException ignored) {
            } finally {
                try (socket) {
                    socket.getInputStream().close();
                } catch (IOException ignored) { }
            }
        }

        return socket;
    }

    public Socket closeSocketAndReconnect(Socket socket) throws IOException {
        //get socket address
        InetAddress address = socket.getInetAddress();
        int port = socket.getPort();
        InetAddress myAddress = socket.getLocalAddress();
        closeSocket(socket);
        return reconnectSocket(address, port, myAddress);
    }

    //receiverAddress, receiverPort, senderAddress
    public Socket reconnectSocket(InetAddress address, int port, InetAddress myAddress) throws IOException {
        Socket sock;
        if (myAddress != null) {
            sock = new Socket();
            sock.bind(new InetSocketAddress(myAddress, 0));

            try {
                sock.connect(new InetSocketAddress(address, port), 8000);
                return sock;
            } catch (SocketTimeoutException ste) {
                throw new ConnectException("RecorderError::error::Socket timeout error (8sec)" + address + ":" + port);
            }
        } else {
            sock = new Socket();

            try {
                sock.connect(new InetSocketAddress(address, port), 8000);
                return sock;
            } catch (SocketTimeoutException ste) {
                throw new ConnectException("RecorderError::error::Socket timeout error (8sec)" + address + ":" + port);
            }
        }
    }
}
