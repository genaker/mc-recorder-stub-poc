package net.genaker.mcrecorder.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/v1/mcrecorderstub")
public class McRecorderEventControllerStub {

    @GetMapping(path = "/smoke-test")
    public ResponseEntity<String> produceSmokeTestEvents() {

        return ResponseEntity.ok("Success ?");
    }
}
