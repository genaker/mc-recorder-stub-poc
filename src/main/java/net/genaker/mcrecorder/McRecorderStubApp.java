package net.genaker.mcrecorder;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class McRecorderStubApp {

    public static void main(String[] args) {

        SpringApplication.run(McRecorderStubApp.class, args);
    }
}
