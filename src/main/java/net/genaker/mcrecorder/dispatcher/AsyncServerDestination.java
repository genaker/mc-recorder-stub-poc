package net.genaker.mcrecorder.dispatcher;


import lombok.extern.slf4j.Slf4j;

import java.net.SocketException;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import static java.lang.Thread.State.BLOCKED;
import static java.lang.Thread.State.TERMINATED;

@Slf4j
public abstract class AsyncServerDestination implements Destination<String, byte[]>{

    protected static final String NAME  ="name:";

    protected BlockingQueue<byte[]> queue = new LinkedBlockingQueue<>();

    private static final int TIME_CONTROL = 60000; //1 min
    private static final int QUEUE_CONTROL = 600;
    private Thread senderThread = null;
    private AtomicBoolean runningSenderThread = new AtomicBoolean(false);
    private AtomicBoolean blockedThread = new AtomicBoolean(false);
    private AtomicLong lastSend = new AtomicLong(0);
    private final AtomicLong threadCount = new AtomicLong(0);

    private CopyOnWriteArrayList<Thread> oldSenderThreads = new CopyOnWriteArrayList<>();


    @Override
    public String send(byte[] data) {
        if(blockedThread.get()) {
            checkOldSenderThreads();
            log.error("Recorder::error::send::produceData::senderThread is blocked");
        }else {
            if(isAliveSenderThread() && isOkSenderThread() > 0) {
                interruptSenderThread();
                if(isOkSenderThread() >= 2) {
                    stop();
                    queue.clear();
                }
            }

            produceData(data);

            if(!isAliveSenderThread()) {
                initSenderThread();
            }
        }

        return null;
    }

    public boolean isRunningSenderThread() {
        return runningSenderThread.get();
    }

    public boolean isAliveSenderThread() {
        return senderThread != null;
    }

    public String getSenderThreadName() {
        return senderThread.getName();
    }

    public void interruptSenderThread() {
        runningSenderThread.set(false);
        if (senderThread != null && !TERMINATED.equals(senderThread.getState())) {
            senderThread.interrupt();
        }

        checkOldSenderThreads();

        oldSenderThreads.add(senderThread);
        senderThread = null;
    }

    private void checkOldSenderThreads() {
        if(!oldSenderThreads.isEmpty()) {
            for(int i = 0; i < oldSenderThreads.size(); i++) {
                Thread oldThread = oldSenderThreads.get(i);
                if(oldThread != null &&  BLOCKED.equals(oldThread.getState())) {
                    blockedThread.set(true);
                } else {
                    oldSenderThreads.remove(i);
                }
            }
        }

        if(oldSenderThreads.isEmpty()) {
            blockedThread.set(false);
        }
    }

    public int queueSize() {
        return queue.size();
    }

    protected int isOkSenderThread() {
        int isOk = 0;
        if(!isRunningSenderThread()) {
            isOk += 1;
        }
        if(lastSend.get() > 0 && (System.currentTimeMillis() - lastSend.get()) > TIME_CONTROL) {
            isOk += 2;
        }
        return isOk;
    }

    protected abstract void sendData(byte [] data) throws SocketException;

    protected abstract int getSocketPort();

    protected abstract String getSocketHostAddress();

    protected abstract void stop();

    private void produceData (byte[] data) {
        //produce data and put in queue for the socket.
        ExecutorService executor = Executors.newSingleThreadExecutor(r -> new Thread(r, this.getClass().getSimpleName()+"::producer::"+threadCount.get()+"::"+getSocketPort()));
        executor.execute(() -> {
            try {
                queue.put(data);
            } catch (InterruptedException e) {
                log.error("Recorder::error::produceData::Error putting in queue:" +e.getMessage()+" "+NAME+getSenderThreadName(), e);
                queue = new LinkedBlockingQueue<>();
                Thread.currentThread().interrupt();
            }
        });
        executor.shutdown();
    }

    private void initSenderThread() {
        //The name of thread is the thread counter + the port of the socket in the moment of thread started
        senderThread = new Thread(this.getClass().getSimpleName()+"::senderThread::"+threadCount.getAndIncrement()+"::"+getSocketPort()) {
            @Override
            public void run(){
                int emtpyCount = 0;
                runningSenderThread.set(true);
                while (runningSenderThread.get()) {
                    if(queue.isEmpty() && emtpyCount >= QUEUE_CONTROL) {
                        runningSenderThread.set(false);//stop running
                    }else if(queue.isEmpty()){
                        sleepIfQueueIsEmpty();
                        emtpyCount++;
                    }else {
                        try {
                            takeDataAndSend();
                        } catch (SocketException e) {
                            log.error("Error sending data through socket", e);

                        }
                        emtpyCount = 0;//reset
                    }
                    lastSend.set(System.currentTimeMillis());
                }
            }

            private void sleepIfQueueIsEmpty() {
                try {
                    int randomNum = ThreadLocalRandom.current().nextInt(50, 100 + 1);
                    Thread.sleep(randomNum);
                } catch (InterruptedException ei) {
                    log.error("Recorder::error::sleepIfQueueIsEmpty::Error in the main thread, sleep interrupted"+ei.getMessage()+" "+NAME+getSenderThreadName(), ei);
                    runningSenderThread.set(false);
                    Thread.currentThread().interrupt();
                }
            }

            private void takeDataAndSend() throws SocketException {
                try {
                    byte [] data = queue.take();
                    log.info("Recorder::info::"+ Thread.currentThread().getName()+" - Recorder send data with length:"+data.length+ " to:"+getSocketHostAddress()+"/"+getSocketPort());
                    sendData(data);
                } catch (InterruptedException e) {
                    log.error("Recorder::error::sleepIfQueueIsEmpty::Thread error taking the queue:" +e.getMessage()+" "+NAME+getSenderThreadName(), e);
                    runningSenderThread.set(false);
                    queue = new LinkedBlockingQueue<>();
                    Thread.currentThread().interrupt();
                }
            }
         };
         senderThread.start();
    }
}