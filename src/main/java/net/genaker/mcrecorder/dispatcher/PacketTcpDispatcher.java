package net.genaker.mcrecorder.dispatcher;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.genaker.mcrecorder.utils.CustomSocketUtils;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;

import static java.util.Objects.nonNull;

@Slf4j
@RequiredArgsConstructor
@Service
public class PacketTcpDispatcher extends AsyncServerDestination{

    private final CustomSocketUtils customSocketUtils;
    private Socket socket;

    public void sendData(byte [] data) throws SocketException {
        if(this.socket!=null && !this.socket.isClosed()) {

            if(!customSocketUtils.socketIsFree(socket)) {
                log.warn("Recorder::warn::sendData::Socket isn't free, we can't write now but we can try anyway: "+ getSenderThreadName());
            }

            try {
                customSocketUtils.putSocketMonitor(socket, CustomSocketUtils.MonitorMode.SERVER);
                customSocketUtils.binaryWriteIntMode(socket, data);
                log.info("Recorder::info::sendData::"+queue.size()+"::flush::"+data.length+"::"+this.getSocketPort());
            } catch (IOException e) {
                log.error("Recorder::error::sendData::Recorder Destination write error : " +e.getMessage(), e);
                //But close it if the client closed the connection.
                if(socket.isClosed() || !socket.isConnected() || socket.isOutputShutdown() ||
                        e.getMessage().contains("Unrecognized SSL message, plaintext connection") ) {
                    assureSocketClosureAndInterruptThread();
                }
            } finally {
                customSocketUtils.removeSocketMonitor(socket, CustomSocketUtils.MonitorMode.SERVER);
            }
        } else {
            this.socket = null;
            throw new SocketException("sending data with no connected socket");
        }
    }

    protected int getSocketPort() {
        return nonNull(socket) ? socket.getPort() : -1;
    }

    protected String getSocketHostAddress() {
        return nonNull(socket) ? socket.getInetAddress().getHostAddress() : "";
    }

    protected void stop() {
        socket = customSocketUtils.agressiveCloseSocket(socket);
        try {
            customSocketUtils.agressiveCloseSocket(new Socket( socket.getInetAddress(),socket.getLocalPort()));
        } catch (IOException e) {
            log.error("Recorder::error::stop::Error closing the socket:"+e.getMessage(), e);
        }
    }

    public void setSocket(Socket socket) {
        this.socket = socket;
    }

    public Socket getSocket() {
        return socket;
    }

    public void assureSocketClosureAndInterruptThread() {
        try {
            socket = customSocketUtils.closeSocket(socket);
            super.interruptSenderThread();
        } catch (Exception e1) {
            log.error("Recorder::error::closeSocket::Error closing the socket:"+e1.getMessage(), e1);
        }
    }
}
