package net.genaker.mcrecorder.dispatcher;

public interface Destination<T, U> {
    T send(U data);
}
