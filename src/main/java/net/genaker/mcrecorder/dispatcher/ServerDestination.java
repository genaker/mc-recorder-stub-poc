package net.genaker.mcrecorder.dispatcher;

public class ServerDestination implements Destination<String, byte[]> {

    final String ip;
    final String port;

    public ServerDestination(String ip, String port) {
        this.ip = ip;
        this.port = port;
    }

    @Override
    public String send(byte[] data) {
        return null;
    }
}
