package net.genaker.mcrecorder.dispatcher;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.UnknownHostException;

@Slf4j
@Service
public class DatagramUdpDispatcher extends AsyncServerDestination {

    private String ip;
    private int port;
    private int timeoutMilis = 500; //0.5  seconds by default

    @Override
    protected void sendData(byte[] data){
        log.debug("Send UDP burst to: " + this.getIp() + ":" + getPort());
        try (DatagramSocket socketUDP = new DatagramSocket()){
            socketUDP.setSoTimeout(timeoutMilis);
            DatagramPacket request = new DatagramPacket(data, data.length, InetAddress.getByName(ip), port);
            socketUDP.send(request);
            log.debug("Recorder::sendData::"+queue.size()+"::socketUDP.send");
        } catch (IOException e) {
            log.error("Recorder::error::sendData::Error sending to recorder:"+e.getMessage(), e);
        }
    }

    protected int getSocketPort() {
        return port;
    }

    protected String getSocketHostAddress() {
        try {
            return InetAddress.getByName(ip).getHostAddress();
        } catch (UnknownHostException e) {
            log.warn("Problem getting host address for ip:"+ip+" "+e.getMessage(), e);
        }
        return ip;
    }

    public DatagramUdpDispatcher setIp(String ip) {
        this.ip = ip;
        return this;
    }

    public DatagramUdpDispatcher setPort(int port) {
        this.port = port;
        return this;
    }

    public String getIp() {
        return ip;
    }

    public int getPort() {
        return port;
    }

    public DatagramUdpDispatcher setTimeout(int timeout) {
        this.timeoutMilis = timeout;
        return this;
    }

    public DatagramUdpDispatcher setReady(boolean ready) {
        if(!ready) {
            ip = null;
            port = -1;
        }
        return this;
    }

    public boolean isReady() {
        return ip != null && port > 0;
    }

    @Override
    public boolean equals(Object obj) {
        if(!(obj instanceof DatagramUdpDispatcher))
            return false;
        if(obj == this)
            return true;
        DatagramUdpDispatcher o = (DatagramUdpDispatcher)obj;

        return o.getPort() == this.getPort() && o.getIp().equals(this.getIp());
    }

    @Override
    protected void stop() {
        //nothing
    }
}
